myApp.controller('mainCtrl', ['$scope', '$http', function ($scope, $http) {
    $scope.showContent = false;
    $scope.data = [];
    $scope.selectedRow = null;
    $scope.values = [
        {name: 'Home Phone Number', id: 0},
        {name: 'Fax Number', id: 1}
    ];
    $http.get('data/sampleJSON.txt').success(function (response) {
        console.log(response);
        $scope.data = response;
        $scope.showContent = false;
        $scope.modal = false;
        $scope.switchStatus = function () {
            $scope.showContent = !$scope.showContent;
        };
        $scope.showModal = function (item) {
            $scope.modal = !$scope.modal;
            $scope.selectedRow = item;
        };
    });
}]);
