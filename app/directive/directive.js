myApp.directive('changeText', function () {
    return {
        restrict: "A",
        link: function (scope, element) {
            element.on('click', function () {
                if (element.hasClass('active')) {
                    element.text("Hide lesson content");
                }
                else {
                    element.text("Show lesson content");
                }
            })
        },
        controller: 'mainCtrl'
    }
});

myApp.directive('customModal', function () {
    return {
        restrict: "E",
        templateUrl: "../../template/custom-modal.html",
        scope: {
            headerName: '=',
            headerLastName: '=',
            street: "=",
            age: "=",
            city: "=",
            state: "=",
            postal: "=",
            homeNumber: "=",
            faxNumber: "="
        },
        controller: 'mainCtrl'
    }
});